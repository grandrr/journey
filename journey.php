<?php

require __DIR__ . '/vendor/autoload.php';

use Journey\BoardCard\BoardCardFactory;
use Journey\Journey\JourneyDefiner;
use Journey\Route\RouteFactory;
use Journey\Transport\TrainFactory;
use Journey\Transport\PlaneFactory;
use Journey\Transport\BusFactory;

$routeFactory = new RouteFactory();

$routeMadridBarcelona = $routeFactory->create('Mardid', 'Barcelona');
$routeBarcelonaGiorna = $routeFactory->create('Barcelona', 'Giorna');
$routeGiornaStockholm = $routeFactory->create('Giorna', 'Stockholm');
$routeStockholmNewYork = $routeFactory->create('Stockholm', 'NewYork');
$routeNewYorkKiev = $routeFactory->create('NewYork', 'Kiev');
$routeKievOdessa = $routeFactory->create('Kiev', 'Odessa');


//$transportFactory = new TransportFactory();
//
//$transportPlaneA = $transportFactory->create(Transport::TYPE_PLANE);
//$transportPlaneB = $transportFactory->create(Transport::TYPE_PLANE);
//$transportTrain = $transportFactory->create(Transport::TYPE_TRAIN);
//$transportBus = $transportFactory->create(Transport::TYPE_BUS);
//$transportBusA = $transportFactory->create(Transport::TYPE_BUS);
//$transportPlaneC = $transportFactory->create(Transport::TYPE_PLANE);

$trainFactory = new TrainFactory();
$transportTrain = $trainFactory->create();

$planeFactory = new PlaneFactory();
$transportPlaneA = $trainFactory->create();
$transportPlaneB = $trainFactory->create();
$transportPlaneC = $trainFactory->create();

$busFactory = new BusFactory();
$transportBus = $busFactory->create();
$transportBusA = $busFactory->create();

$transportPlaneA->setRoute($routeStockholmNewYork);
$transportPlaneB->setRoute($routeGiornaStockholm);
$transportBus->setRoute($routeMadridBarcelona);
$transportTrain->setRoute($routeBarcelonaGiorna);
$transportBusA->setRoute($routeKievOdessa);
$transportPlaneC->setRoute($routeNewYorkKiev);

$cardFactory = new BoardCardFactory();

$card1 = $cardFactory->create($transportPlaneC, '12A');
$card2 = $cardFactory->create($transportTrain, '12');
$card3 = $cardFactory->create($transportPlaneB, '3A');
$card4 = $cardFactory->create($transportPlaneA, '45F');
$card5 = $cardFactory->create($transportBus, '23');
$card6 = $cardFactory->create($transportBusA, '4');

$journeyFactory = new \Journey\Journey\JourneyFactory();
$journey = $journeyFactory->create();

$pointFactory = new \Journey\Journey\PointFactory();

$journeyDefiner = new JourneyDefiner($journey, $pointFactory);

$journeyDefiner->addBoardCard($card1)
    ->addBoardCard($card2)
    ->addBoardCard($card3)
    ->addBoardCard($card4)
    ->addBoardCard($card5)
    ->addBoardCard($card6)
;

$journey = $journeyDefiner->getJourney();

$journeyViewList = new \Journey\Journey\JourneyViewList();

$view = $journeyViewList->view($journey);

print_r($view);
echo PHP_EOL;


