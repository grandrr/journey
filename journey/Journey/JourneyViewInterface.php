<?php

namespace Journey\Journey;

/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:56
 */
interface JourneyViewInterface
{
    /**
     * @param JourneyInterface $journey
     * @return string
     */
    public function view(JourneyInterface $journey): string;
}