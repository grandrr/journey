<?php

namespace Journey\Journey;

use Journey\BoardCard\BoardCard;

/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:56
 */
class JourneyDefiner implements JourneyDefinerInterface
{

    /**
     * @var array
     */
    protected $undefinedBoardCards = array();

    /**
     * @var JourneyInterface
     */
    protected $journey;

    /**
     * @var PointFactoryInterface
     */
    protected $pointFactory;

    /**
     * JourneyDefiner constructor.
     * @param JourneyInterface      $journey
     * @param PointFactoryInterface $pointFactory
     */
    public function __construct(JourneyInterface $journey, PointFactoryInterface $pointFactory)
    {
        $this->journey = $journey;
        $this->pointFactory = $pointFactory;
    }

    /**
     * @param BoardCard $boardCard
     * @return JourneyDefinerInterface
     */
    public function addBoardCard(BoardCard $boardCard): JourneyDefinerInterface
    {
        if ($this->journey->getCountPoints() == 0) {
            $point = $this->createPoint($boardCard);

            $this->journey->addPointToBegin($point);

            return $this;
        }

        if ($this->addPreviousPointJourney($boardCard)) {
            return $this;
        } else {
            if ($this->addNextPointJourney($boardCard)) {
                return $this;
            }
        }

        $this->undefinedBoardCards[] = $boardCard;

        return $this;
    }

    /**
     * @return JourneyInterface
     */
    public function getJourney(): JourneyInterface
    {
        return $this->journey;
    }

    /**
     * @return bool
     */
    protected function addFromBoardCard()
    {
        /**
         * @var $boardCard BoardCard
         */
        foreach ($this->undefinedBoardCards as $keyBoardCard => $boardCard) {
            if ($this->addPreviousPointJourney($boardCard)) {
                unset($this->undefinedBoardCards[$keyBoardCard]);
            } else {
                if ($this->addNextPointJourney($boardCard)) {
                    unset($this->undefinedBoardCards[$keyBoardCard]);
                }
            }
        }

        return true;
    }

    /**
     * @param BoardCard $boardCard
     * @return bool
     */
    protected function addPreviousPointJourney(BoardCard $boardCard)
    {
        /**
         * $@var $firstPoint Point
         */
        $firstPoint = $this->journey->getFirstPoint();

        $cardArrivalName = $boardCard->getTransport()->getRoute()->getArrival();
        $firstPointDeparture = $firstPoint->getDeparted();

        if ($cardArrivalName == $firstPointDeparture) {
            $point = $this->createPoint($boardCard);

            $this->journey->addPointToBegin($point);

            $this->addFromBoardCard();

            return true;
        }

        return false;
    }

    /**
     * @param BoardCard $boardCard
     * @return bool
     */
    protected function addNextPointJourney(BoardCard $boardCard)
    {
        $lastPoint = $this->journey->getLastPoint();

        $lastPointArrival = $lastPoint->getArrival();
        $boardCardDeparted = $boardCard->getTransport()->getRoute()->getDeparted();

        if ($lastPointArrival == $boardCardDeparted) {
            $point = $this->createPoint($boardCard);

            $this->journey->addPointToEnd($point);

            $this->addFromBoardCard();

            return true;
        }

        return false;
    }

    /**
     * @param BoardCard $boardCard
     * @return Point
     */
    protected function createPoint(BoardCard $boardCard)
    {
        $transportType = get_class($boardCard->getTransport());
        $seatNumber = $boardCard->getSeatNumber();

        $route = $boardCard->getTransport()->getRoute();

        $departed = $route->getDeparted();
        $arrival = $route->getArrival();

        $point = $this->pointFactory->create(
            $transportType,
            $seatNumber,
            $departed,
            $arrival
        );

        return $point;
    }
}