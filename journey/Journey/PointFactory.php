<?php

namespace Journey\Journey;

/**
 * Class PointFactory
 * @package Journey\BoardCard
 */
class PointFactory implements PointFactoryInterface
{
    /**
     * @param $transportType
     * @param $seatNumber
     * @param $departed
     * @param $arrival
     * @return Point
     */
    public function create(string $transportType, string $seatNumber, string $departed, string $arrival): Point
    {
        $point = new Point($transportType, $seatNumber, $departed, $arrival);

        return $point;
    }
}