<?php

namespace Journey\Journey;

/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:56
 */
class Point implements PointInterface
{

    /**
     * @var string
     */
    protected $transportType;

    /**
     * @var string
     */
    protected $seatNumber;

    /**
     * @var string
     */
    protected $departed;

    /**
     * @var string
     */
    protected $arrival;

    /**
     * Point constructor.
     * @param string $transportType
     * @param string $seatNumber
     * @param string $departed
     * @param string $arrival
     */
    public function __construct(string $transportType, string $seatNumber, string $departed, string $arrival)
    {
        $this->transportType = $transportType;
        $this->seatNumber = $seatNumber;
        $this->departed = $departed;
        $this->arrival = $arrival;
    }

    /**
     * @return string
     */
    public function getTransportType(): string
    {
        return $this->transportType;
    }

    /**
     * @return string
     */
    public function getSeatNumber(): string
    {
        return $this->seatNumber;
    }

    /**
     * @return string
     */
    public function getDeparted(): string
    {
        return $this->departed;
    }

    /**
     * @return string
     */
    public function getArrival(): string
    {
        return $this->arrival;
    }
}