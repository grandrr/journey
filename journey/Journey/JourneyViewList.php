<?php

namespace Journey\Journey;

/**
 * Class JourneyViewList
 * @package Journey\Journey
 */
class JourneyViewList implements JourneyViewInterface
{
    /**
     * @var string
     */
    protected $format = 'get %s from %s to %s, take a seat %s';

    /**
     * @param JourneyInterface $journey
     * @return string
     */
    public function view(JourneyInterface $journey): string
    {
        $result = '';
        /**
         * @var $point Point
         */
        foreach ($journey->getPoints() as $point) {
            $message = sprintf(
                $this->format,
                $point->getTransportType(),
                $point->getDeparted(),
                $point->getArrival(),
                $point->getSeatNumber()
            );

            $result .= $message.PHP_EOL;
        }

        if ($result == '') {
            $result = 'In you journey not exist points';
        } else {
            $result .= 'This was your final destination';
        }

        return $result;
    }
}