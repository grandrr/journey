<?php

namespace Journey\Journey;

/**
 * Class Journey
 * @package Journey\Journey
 */
class Journey implements JourneyInterface
{

    /**
     * @var array
     */
    protected $points = array();

    /**
     * @param Point $point
     * @return JourneyInterface
     */
    public function addPointToBegin(Point $point): JourneyInterface
    {
        array_unshift($this->points, $point);

        return $this;
    }

    /**
     * @param Point $point
     * @return JourneyInterface
     */
    public function addPointToEnd(Point $point): JourneyInterface
    {
        $this->points[] = $point;

        return $this;
    }

    /**
     * @return Point
     */
    public function getFirstPoint(): Point
    {
        $pointsKeys = array_keys($this->points);

        return $this->points[$pointsKeys[0]];
    }

    /**
     * @return Point
     */
    public function getLastPoint(): Point
    {
        $lastKeyPoint = count($this->points) - 1;

        return $this->points[$lastKeyPoint];
    }

    /**
     * @return int
     */
    public function getCountPoints(): int
    {
        return count($this->points);
    }

    /**
     * @return array
     */
    public function getPoints(): array
    {
        return $this->points;
    }
}