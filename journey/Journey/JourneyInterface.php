<?php

namespace Journey\Journey;

/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:56
 */
interface JourneyInterface
{

    /**
     * @param Point $point
     * @return JourneyInterface
     */
    public function addPointToBegin(Point $point): JourneyInterface;

    /**
     * @param Point $point
     * @return JourneyInterface
     */
    public function addPointToEnd(Point $point): JourneyInterface;

    /**
     * @return Point
     */
    public function getFirstPoint(): Point;

    /**
     * @return Point
     */
    public function getLastPoint(): Point;

    /**
     * @return int
     */
    public function getCountPoints(): int;

    /**
     * @return array
     */
    public function getPoints(): array;
}