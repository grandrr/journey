<?php

namespace Journey\Journey;


/**
 * Class JourneyFactory
 * @package Journey\BoardCard
 */
class JourneyFactory implements JourneyFactoryInterface
{
    /**
     * @return JourneyInterface
     */
    public function create(): JourneyInterface
    {
        $journey = new Journey();

        return $journey;
    }
}