<?php

namespace Journey\Journey;

use Journey\BoardCard\BoardCard;

/**
 * Interface JourneyDefinerInterface
 * @package Journey\Journey
 */
interface JourneyDefinerInterface
{

    /**
     * @param BoardCard $boardCard
     * @return JourneyDefinerInterface
     */
    public function addBoardCard(BoardCard $boardCard): JourneyDefinerInterface;

    /**
     * @return JourneyInterface
     */
    public function getJourney(): JourneyInterface;
}