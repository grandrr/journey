<?php

namespace Journey\Journey;

/**
 * Class PointFactory
 * @package Journey\BoardCard
 */
interface PointFactoryInterface
{
    /**
     * @param $transportType
     * @param $seatNumber
     * @param $departed
     * @param $arrival
     * @return Point
     */
    public function create(string $transportType, string $seatNumber, string $departed, string $arrival): Point;
}