<?php

namespace Journey\Journey;

/**
 * Interface PointInterface
 * @package Journey\Journey
 */
interface PointInterface
{
    /**
     * @return string
     */
    public function getTransportType(): string;

    /**
     * @return string
     */
    public function getSeatNumber(): string;

    /**
     * @return string
     */
    public function getDeparted(): string;

    /**
     * @return string
     */
    public function getArrival(): string;
}