<?php

namespace Journey\Journey;

/**
 * Class JourneyFactory
 * @package Journey\BoardCard
 */
interface JourneyFactoryInterface
{
    /**
     * @return JourneyInterface
     */
    public function create() : JourneyInterface;
}