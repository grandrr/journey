<?php
/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:04
 */

namespace Journey\Transport;


use Journey\Route\Route;

/**
 * Class Transport
 * @package Journey\Transport
 */
abstract class Transport implements TransportInterface
{
    /**
     * @var Route
     */
    protected $route;

    /**
     * @param Route $route
     * @return TransportInterface
     */
    public function setRoute(Route $route): TransportInterface
    {
        $this->route = $route;

        return $this;
    }

    /**
     * @return Route
     */
    public function getRoute(): Route
    {
        return $this->route;
    }
}