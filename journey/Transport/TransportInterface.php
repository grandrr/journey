<?php

namespace Journey\Transport;

use Journey\Route\Route;

/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 18:56
 */
interface TransportInterface
{
    /**
     * @param Route $route
     * @return TransportInterface
     */
    public function setRoute(Route $route): TransportInterface;

    /**
     * @return Route
     */
    public function getRoute(): Route;
}