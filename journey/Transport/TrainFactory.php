<?php
/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:04
 */

namespace Journey\Transport;

/**
 * Class Transport
 * @package Journey\Transport
 */
class TrainFactory implements TransportFactoryInterface
{
    /**
     * @return TransportInterface
     */
    public function create(): TransportInterface
    {
        $transport = new Train();

        return $transport;
    }
}