<?php
/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:04
 */

namespace Journey\Transport;

/**
 * Class Transport
 * @package Journey\Transport
 */
interface TransportFactoryInterface
{

    /**
     * @param $type
     * @return TransportInterface
     */
    public function create(): TransportInterface;
}