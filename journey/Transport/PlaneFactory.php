<?php
/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:04
 */

namespace Journey\Transport;

/**
 * Class PlaneFactory
 * @package Journey\Transport
 */
class PlaneFactory implements TransportFactoryInterface
{
    /**
     * @return TransportInterface
     */
    public function create(): TransportInterface
    {
        $transport = new Plane();

        return $transport;
    }
}