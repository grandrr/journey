<?php
/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:01
 */

namespace Journey\Route;

/**
 * Class Route
 * @package Journey\Route
 */
class RouteFactory implements RouteFactoryInterface
{
    /**
     * @param $departed
     * @param $arrival
     * @return Route
     */
    public function create(string $departed, string $arrival): Route
    {
        $route = new Route($departed, $arrival);

        return $route;
    }
}