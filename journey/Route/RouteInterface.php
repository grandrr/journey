<?php
/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:01
 */

namespace Journey\Route;

/**
 * Class Route
 * @package Journey\Route
 */
interface RouteInterface
{
    /**
     * @return string
     */
    public function getDeparted(): string;

    /**
     * @return string
     */
    public function getArrival(): string;
}