<?php
/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:01
 */

namespace Journey\Route;

/**
 * Class Route
 * @package Journey\Route
 */
class Route implements RouteInterface
{
    /**
     * @var string
     */
    protected $departed;

    /**
     * @var string
     */
    protected $arrival;

    /**
     * Route constructor.
     * @param string $departed
     * @param string $arrival
     */
    public function __construct($departed, $arrival)
    {
        $this->departed = $departed;
        $this->arrival = $arrival;
    }

    /**
     * @return string
     */
    public function getDeparted(): string
    {
        return $this->departed;
    }

    /**
     * @return string
     */
    public function getArrival(): string
    {
        return $this->arrival;
    }
}