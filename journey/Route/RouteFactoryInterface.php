<?php
/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:01
 */

namespace Journey\Route;

/**
 * Class Route
 * @package Journey\Route
 */
interface RouteFactoryInterface
{
    /**
     * @param string $departed
     * @param string $arrival
     * @return Route
     */
    public function create(string $departed, string $arrival): Route;
}