<?php

namespace Journey\Tests;

use Journey\BoardCard\BoardCardFactory;
use Journey\Journey\JourneyDefiner;
use Journey\Journey\JourneyFactory;
use Journey\Journey\JourneyViewList;
use Journey\Journey\PointFactory;
use Journey\Route\RouteFactory;
use Journey\Transport\Transport;
use Journey\Transport\TransportFactory;
use PHPUnit\Framework\TestCase;

/**
 * Created by PhpStorm.
 * User: user01
 * Date: 16.07.17
 * Time: 21:00
 */
class JourneyDefinerTest extends TestCase
{

    /**
     *
     */
    const CITY_MADRID = 'Madrid';
    /**
     *
     */
    const CITY_BARCELONA = 'Barcelona';
    /**
     *
     */
    const CITY_GIORNA = 'Giorna';
    /**
     *
     */
    const CITY_STOCKHOLM = 'Stockholm';
    /**
     *
     */
    const CITY_NEW_YORK = 'NewYork';
    /**
     *
     */
    const CITY_KIEV = 'Kiev';
    /**
     *
     */
    const CITY_ODESSA = 'Odessa';

    const CITY_PARIS = 'Paris';

    const CITY_CHICAGO = 'Chicago';


    /**
     *
     */
    public function testJourneyDefiner()
    {
        $routeFactory = new RouteFactory();

        $routeMadridBarcelona = $routeFactory->create(self::CITY_MADRID, self::CITY_BARCELONA);
        $routeBarcelonaGiorna = $routeFactory->create(self::CITY_BARCELONA, self::CITY_GIORNA);
        $routeGiornaStockholm = $routeFactory->create(self::CITY_GIORNA, self::CITY_STOCKHOLM);
        $routeStockholmNewYork = $routeFactory->create(self::CITY_STOCKHOLM, self::CITY_NEW_YORK);
        $routeNewYorkKiev = $routeFactory->create(self::CITY_NEW_YORK, self::CITY_KIEV);
        $routeKievOdessa = $routeFactory->create(self::CITY_KIEV, self::CITY_ODESSA);


        $transportFactory = new TransportFactory();

        $transportPlaneA = $transportFactory->create(Transport::TYPE_PLANE);
        $transportPlaneB = $transportFactory->create(Transport::TYPE_PLANE);
        $transportTrain = $transportFactory->create(Transport::TYPE_TRAIN);
        $transportBus = $transportFactory->create(Transport::TYPE_BUS);
        $transportBusA = $transportFactory->create(Transport::TYPE_BUS);
        $transportPlaneC = $transportFactory->create(Transport::TYPE_PLANE);

        $transportPlaneA->setRoute($routeStockholmNewYork);
        $transportPlaneB->setRoute($routeGiornaStockholm);
        $transportBus->setRoute($routeMadridBarcelona);
        $transportTrain->setRoute($routeBarcelonaGiorna);
        $transportBusA->setRoute($routeKievOdessa);
        $transportPlaneC->setRoute($routeNewYorkKiev);


        $cardFactory = new BoardCardFactory();

        $card1 = $cardFactory->create($transportPlaneC, '12A');
        $card2 = $cardFactory->create($transportTrain, '12');
        $card3 = $cardFactory->create($transportPlaneB, '3A');
        $card4 = $cardFactory->create($transportPlaneA, '45F');
        $card5 = $cardFactory->create($transportBus, '23');
        $card6 = $cardFactory->create($transportBusA, '4');


        $journeyFactory = new JourneyFactory();

        $journey = $journeyFactory->create();

        $pointFactory = new PointFactory();

        $journeyDefiner = new JourneyDefiner($journey, $pointFactory);

        $journeyDefiner->addBoardCard($card1)
            ->addBoardCard($card2)
            ->addBoardCard($card3)
            ->addBoardCard($card4)
            ->addBoardCard($card5)
            ->addBoardCard($card6);

        $journey = $journeyDefiner->getJourney();

        $journeyViewList = new JourneyViewList();

        $view = $journeyViewList->view($journey);

        $checkList = file_get_contents(__DIR__.'/assertJourneyList.txt');

        $this->assertEquals($view, $checkList);
    }

    /**
     *
     */
    public function testJourneyDefinerWithCartOutOfChain()
    {
        $routeFactory = new RouteFactory();

        $routeMadridBarcelona = $routeFactory->create(self::CITY_MADRID, self::CITY_BARCELONA);
        $routeBarcelonaGiorna = $routeFactory->create(self::CITY_BARCELONA, self::CITY_GIORNA);
        $routeGiornaStockholm = $routeFactory->create(self::CITY_GIORNA, self::CITY_STOCKHOLM);
        $routeStockholmNewYork = $routeFactory->create(self::CITY_STOCKHOLM, self::CITY_NEW_YORK);
        $routeNewYorkKiev = $routeFactory->create(self::CITY_NEW_YORK, self::CITY_KIEV);
        $routeKievOdessa = $routeFactory->create(self::CITY_KIEV, self::CITY_ODESSA);
        $routeParisChicago = $routeFactory->create(self::CITY_PARIS, self::CITY_CHICAGO);


        $transportFactory = new TransportFactory();

        $transportPlaneA = $transportFactory->create(Transport::TYPE_PLANE);
        $transportPlaneB = $transportFactory->create(Transport::TYPE_PLANE);
        $transportTrain = $transportFactory->create(Transport::TYPE_TRAIN);
        $transportBus = $transportFactory->create(Transport::TYPE_BUS);
        $transportBusA = $transportFactory->create(Transport::TYPE_BUS);
        $transportPlaneC = $transportFactory->create(Transport::TYPE_PLANE);
        $transportPlaneD = $transportFactory->create(Transport::TYPE_PLANE);

        $transportPlaneA->setRoute($routeStockholmNewYork);
        $transportPlaneB->setRoute($routeGiornaStockholm);
        $transportBus->setRoute($routeMadridBarcelona);
        $transportTrain->setRoute($routeBarcelonaGiorna);
        $transportBusA->setRoute($routeKievOdessa);
        $transportPlaneC->setRoute($routeNewYorkKiev);
        $transportPlaneD->setRoute($routeParisChicago);


        $cardFactory = new BoardCardFactory();

        $card1 = $cardFactory->create($transportPlaneC, '12A');
        $card2 = $cardFactory->create($transportTrain, '12');
        $card3 = $cardFactory->create($transportPlaneB, '3A');
        $card4 = $cardFactory->create($transportPlaneA, '45F');
        $card5 = $cardFactory->create($transportBus, '23');
        $card6 = $cardFactory->create($transportBusA, '4');
        $card7 = $cardFactory->create($transportPlaneD, '4');


        $journeyFactory = new JourneyFactory();

        $journey = $journeyFactory->create();

        $pointFactory = new PointFactory();

        $journeyDefiner = new JourneyDefiner($journey, $pointFactory);

        $journeyDefiner->addBoardCard($card1)
            ->addBoardCard($card2)
            ->addBoardCard($card3)
            ->addBoardCard($card4)
            ->addBoardCard($card5)
            ->addBoardCard($card6)
            ->addBoardCard($card7)
        ;

        $journey = $journeyDefiner->getJourney();

        $journeyViewList = new JourneyViewList();

        $view = $journeyViewList->view($journey);

        $checkList = file_get_contents(__DIR__.'/assertJourneyList.txt');

        $this->assertEquals($view, $checkList);
    }
}