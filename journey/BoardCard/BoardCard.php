<?php

namespace Journey\BoardCard;

use Journey\Transport\Transport;
use Journey\Transport\TransportInterface;

/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 18:55
 */
class BoardCard implements BoardCardInterface
{

    /**
     * @var TransportInterface
     */
    protected $transport;

    /**
     * @var string
     */
    protected $seatNumber;

    /**
     * BoardCard constructor.
     * @param TransportInterface $transport
     * @param string             $seatNumber
     */
    public function __construct(TransportInterface $transport, $seatNumber)
    {
        $this->transport = $transport;
        $this->seatNumber = $seatNumber;
    }

    /**
     * @return TransportInterface
     */
    public function getTransport(): TransportInterface
    {
        return $this->transport;
    }

    /**
     * @return string
     */
    public function getSeatNumber(): string
    {
        return $this->seatNumber;
    }
}