<?php

namespace Journey\BoardCard;

use Journey\Transport\TransportInterface;

/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 18:55
 */
interface BoardCardFactoryInterface
{
    /**
     * @param TransportInterface $transport
     * @param string             $seatNumber
     * @return BoardCard
     */
    public function create(TransportInterface $transport, string $seatNumber): BoardCard;
}