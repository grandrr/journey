<?php
/**
 * Created by PhpStorm.
 * User: user01
 * Date: 15.07.17
 * Time: 19:11
 */

namespace Journey\BoardCard;

use Journey\Transport\TransportInterface;

/**
 * Interface BoardCardInterface
 * @package Journey\BoardCard
 */
interface BoardCardInterface
{
    /**
     * @return TransportInterface
     */
    public function getTransport(): TransportInterface;

    /**
     * @return string
     */
    public function getSeatNumber(): string;
}